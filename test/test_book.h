#include "../src/book.h"

TEST (Book, normal) {
    Book k("Object-Oriented Programming", "Patrick", 1108);
    ASSERT_EQ("Object-Oriented Programming", k.title());
    ASSERT_EQ("Patrick", k.author());
    ASSERT_EQ(1108, k.price());
}

TEST (Book, equal) {
    Book k1("Object-Oriented Programming", "Patrick", 1108);
    Book k2("Object-Oriented Programming", "Patrick", 1108);
	Book k3("Design Patterns", "YCC", 1500);
    ASSERT_TRUE(k1 == k2);
	ASSERT_FALSE(k1 == k3);
}

TEST(Book, CopyAssignment) {
  Book k1("Object-Oriented Programming", "Patrick", 1108);
  Book k2;
  k2 = k1;
  ASSERT_EQ("Object-Oriented Programming", k2.title());
  ASSERT_EQ("Patrick", k2.author());
  ASSERT_EQ(1108, k2.price());
}

TEST(Book, Price) {
  ASSERT_ANY_THROW(Book k1("Object-Oriented Programming", "Patrick", -1108));
}
