#include "../src/template.h"

TEST(Template, Fill_Int) {
  int a[3];
  fill<int>(a,4,1);
  ASSERT_EQ(1,a[0]);
  ASSERT_EQ(1,a[1]);
  ASSERT_EQ(1,a[2]);
  ASSERT_EQ(1,a[3]);
}

TEST(Template, Fill_Double) {
  double a[2];
  fill<double>(a,3,1.2);
  ASSERT_EQ(1.2,a[0]);
  ASSERT_EQ(1.2,a[1]);
  ASSERT_EQ(1.2,a[2]);
}

TEST(Template, Fill_char) {
  char a[2];
  fill(a,3,'b');
  ASSERT_EQ('b',a[0]);
  ASSERT_EQ('b',a[1]);
  ASSERT_EQ('b',a[2]);
}

TEST(Template,Reverse_Int) {
  int a[] = {1,2,3};
  reverse(a,3);
  ASSERT_EQ(a[0],3);
  ASSERT_EQ(a[1],2);
  ASSERT_EQ(a[2],1);
}

TEST(Template, Reverse_Double) {
  double a[] = {1.0,2.0,3.0};
  reverse(a,3);
  ASSERT_EQ(a[0],3.0);
  ASSERT_EQ(a[1],2.0);
  ASSERT_EQ(a[2],1.0);
}

TEST(Template, Reverse_char) {
  char a[] = {'a','b','c'};
  reverse(a,3);
  ASSERT_EQ('c',a[0]);
  ASSERT_EQ('b',a[1]);
  ASSERT_EQ('a',a[2]);
}

TEST(Template, DeepCopy_Int) {
  int a[3] = {1,2,3};
  int *b;
  b=deepCopy(a,3);
  ASSERT_EQ(1,b[0]);
  ASSERT_EQ(2,b[1]);
  ASSERT_EQ(3,b[2]);
}

TEST(Template, DeepCopy_Double) {
  double a[3] = {1.0,2.0,3.0};
  double *b;
  b=deepCopy(a,3);
  ASSERT_EQ(1.0,b[0]);
  ASSERT_EQ(2.0,b[1]);
  ASSERT_EQ(3.0,b[2]);
}

TEST(Template, DeepCopy_char) {
  char a[] = {'a','b','c'};
  char *b;
  b=deepCopy(a,3);
  ASSERT_EQ('a',a[0]);
  ASSERT_EQ('b',a[1]);
  ASSERT_EQ('c',a[2]);
}
