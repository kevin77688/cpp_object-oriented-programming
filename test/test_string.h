#include "../src/vector_io.h"

#include <fstream>

TEST (string, c_str) {
  char cs[] = "hello, string!";
  ASSERT_EQ('h', cs[0]);
  ASSERT_EQ('!', cs[13]);
  ASSERT_EQ('\0', cs[14]);
  ASSERT_EQ(15, sizeof(cs)/sizeof(char));
}

TEST (string, string) {
  std::string cpps="hello, string!";
  int i = 3;
  int j(3);
  ASSERT_EQ('h', cpps[0]);
  ASSERT_EQ('!', cpps[13]);
  ASSERT_EQ('\0', cpps[14]);
  ASSERT_EQ(14, cpps.length());
}

TEST (string, pullVectorFromString) {
  std::string s = "            2 [1,2]";
  int dim;
  double* vec;
  vec = pullVectorFromString(s, &dim);
  ASSERT_EQ(2, dim);
  ASSERT_NEAR(1, vec[0], 0.001);
  ASSERT_NEAR(2, vec[1], 0.001);
  delete [] vec;
}

TEST (string, pullVectorFromString_2) {
  std::string s = "3 [1,2,3]";
  int dim;
  double* a = pullVectorFromString(s, &dim);
  ASSERT_EQ(3, dim);
  ASSERT_NEAR(1, a[0], 0.001);
  ASSERT_NEAR(2, a[1], 0.001);
  ASSERT_NEAR(3, a[2], 0.001);
  delete [] a;
}

TEST(string, pullContinueFromString){
  std::string input ="c\n";
  ASSERT_TRUE(pullContinueFromString(input));

}
TEST(string, pullContinueFromString2){
  std::string input ="xcxc\n";
  ASSERT_FALSE(pullContinueFromString(input));

}
