#include "../src/dot.h"
#include "../src/vector.h"

#include <cmath>

TEST (DotProduct, Vectorfirst) {
  double u[] = {1,1};
  double v[] = {0,1};
  Vector x(u,2);
  Vector y(v,2);
  double dp = dotProduct(x,y);
  ASSERT_EQ(1, dp);
  dp = dotProduct(x,y);
  ASSERT_EQ(1, dp);
}

TEST(DotProduct, centroid) {
  double v1[] = {0,0};
  double v2[] = {3,4};
  double v3[] = {0,4};
  double v4[] = {3,0};
  Vector a( v1, 2);
  Vector b( v2, 2);
  Vector c( v3, 2);
  Vector d( v4, 2);
  Vector vertices[] = {a,b,c,d};
  Vector oo = centroid(vertices,4);
  ASSERT_EQ(1.5,oo.at(1));
  ASSERT_EQ(2,oo.at(2));
  Vector r = a - oo;
  ASSERT_EQ(-1.5,r.at(1));
  ASSERT_EQ(-2,r.at(2));
  double ang = angle(r, a-oo);
  ASSERT_EQ(0, ang);
  ASSERT_EQ(M_PI, angle(r, b-oo));
  ASSERT_TRUE(pointingOut(r,b-oo));
  ASSERT_FALSE(pointingOut(r,c-oo));
  ASSERT_NEAR(1.411*M_PI,angle(r,c-oo),0.01);
}
