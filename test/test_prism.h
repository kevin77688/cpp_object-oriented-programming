#include "../src/vector.h"
#include "../src/polygon.h"
#include "../src/circle.h"
#include "../src/shape.h"
#include "../src/prism.h"
#include <cmath>

TEST(PrismTest, Triangular_Prism) {
  double v1[] = {0,0};
  double v2[] = {3,0};
  double v3[] = {3,4};
  Vector vertices[] = { Vector(v1,2), Vector(v2,2), Vector(v3,3) };
  Shape *s = new Polygon (vertices, 3);
  Prism p(s,2);
  ASSERT_EQ(2,p.height());
  ASSERT_EQ(12,p.volume());
  ASSERT_EQ(36,p.surfaceArea());
}

TEST(PrismTest, Rectangular_Prism) {
  double v1[] = {0,0};
  double v2[] = {3,0};
  double v3[] = {3,4};
  double v4[] = {0,4};
  Vector vertices[] = {Vector(v1,2),Vector(v2,2),Vector(v3,2),Vector(v4,2)};
  Shape *s = new Polygon (vertices,4);
  Prism p (s,4);
  ASSERT_EQ(48, p.volume());
  ASSERT_EQ(4, p.height());
  ASSERT_EQ(80, p.surfaceArea());
}

TEST (PrismTest, Circular_Prism) {
  double r = 3;
  double a[] = {0,0};
  Vector o(a,2);
  Shape * s = new Circle(o,r);
  Prism p(s,2);
  ASSERT_EQ(2,p.height());
  ASSERT_EQ(18*M_PI,p.volume());
  ASSERT_EQ(30*M_PI,p.surfaceArea());
}
