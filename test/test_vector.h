#include "../src/vector.h"
#include "../src/vector_io.h"
#include <string>

TEST (Vector, createVector) {
  double a[] = {1,2};
  int dim = 2;
  Vector u(a, dim);
  ASSERT_EQ(2, u.dim());
  ASSERT_EQ(1, u.at(1));
  ASSERT_EQ(2, u.at(2));
}

TEST (Vector, pullVectorFromString) {
  std::string s = "            2 [1,2]";
  Vector * vec = pullVectorFromString(s);
  ASSERT_EQ(2, vec->dim());
  ASSERT_NEAR(1, vec->at(1), 0.001);
  ASSERT_NEAR(2, vec->at(2), 0.001);
  delete vec;
}

TEST (Vector, substract) {
  double a[] = {0,0};
  double b[] = {3,0};
  int dim = 2;
  Vector u(a, dim);
  Vector v(b, dim);
  Vector w(u-v);
  ASSERT_EQ(2, w.dim());
  ASSERT_EQ(-3, w.at(1));
  ASSERT_EQ(0, w.at(2));
}

TEST (Vector, copyassignment){
  double a[] = {1,2};
  Vector u(a,2);
  Vector v;
  v = u;
  ASSERT_EQ(1, v.at(1));
  ASSERT_EQ(2, v.at(2));
  ASSERT_EQ(2, v.dim());
}

TEST (Vector, thisPointer) {
  Vector u;
  ASSERT_EQ(&u, u.getThis());
}

TEST (Vector, triangleArea){
  double v1[] = {0,0};
  double v2[] = {3,0};
  double v3[] = {3,4};
  Vector a( v1, 2);
  Vector b( v2, 2);
  Vector c( v3, 2);
  ASSERT_EQ(6, triangleArea(a,b,c));
}
