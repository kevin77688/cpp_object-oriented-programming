#include <gtest/gtest.h>

// #include "test_dot.h"
// #include "test_string.h"
// #include "test_vector.h"
// #include "test_polygon.h"
// #include "test_bubblesort.h"
// #include "test_template.h"
// #include "test_book.h"
// #include "test_shoppingcart.h"
// #include "test_prism.h"
// #include "test_shape.h"
// #include "test_svg_shape.h"
#include "ut_student.h"

int main( int argc , char **argv )
{
    testing :: InitGoogleTest( &argc , argv ) ;
    return RUN_ALL_TESTS( ) ;
}
