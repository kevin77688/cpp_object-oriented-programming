#ifndef BUBBLESORT_H
#define BUBBLESORT_H

template<typename T>
void swap(T & a, T & b) {
  T temp = a;
  a=b;
  b=temp;
}

template<typename T>
void bubbleSort(T a, T b) {
  int n = b-a;
  for(int i = n-1; i > 0 ; i--) {
    for(int j=0;j<i;j++) {
      if(a[j]>a[j+1]) {
        swap(a[j],a[j+1]);
      }
    }
  }
}

template<typename T>
bool lessThan (T a, T b){
  return a < b;
}

template<typename T>
bool greaterThan (T a, T b){
  return a > b;
}

template<typename T, typename C>
void bubbleSort(T a, T b, C comp) {
  int n = b-a;
  for(int i = n-1; i > 0; i--) {
    for(int j=0;j<i;j++) {
      if (comp(a[j],a[j+1])) {
        swap(a[j],a[j+1]);
      }
    }
  }
}
#endif
