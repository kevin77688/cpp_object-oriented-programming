#ifndef SVG_SHAPE_H
#define SVG_SHAPE_H

class SvgShape {
    public:
        SvgShape(){

        }
        void setFillColor(std::string _fillColor) {
          fill = _fillColor;
        }
        void setStroke(double _strokeWidth, std::string _strokeColor) {
          width = _strokeWidth;
          strokeColor = _strokeColor;
        }

        virtual std::string toSVG() const = 0;


        double width = 0;
        std::string fill = "black";
        std::string strokeColor = "black";
    };

    void setSvgShapeStyle(SvgShape * shape, double _strokeWidth, std::string _strokeColor, std::string _fillColor){
        shape->setStroke(_strokeWidth,_strokeColor);
        shape->setFillColor(_fillColor);
    }

    std::string makeSvgOutput(int width, int height, std::vector<SvgShape *> const & shape){
      std::string s;
      s += "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"" + std::to_string(width) + "\" height=\"" + std::to_string(height) + "\" viewBox=\"0 0 100 100\">";
      for(SvgShape * i:shape)
        s += i->toSVG();
      s += "</svg>";
      return s;
    }

#endif
