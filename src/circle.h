#ifndef CIRCLE_H
#define CIRCLE_H
#include "./vector.h"
#include <cmath>
#include "shape.h"

class Circle : public Shape {
public:
  static Circle create(double x, double y, double r) {
    double c[] = {x,y};
    Vector v (c,2);
    return Circle (v,r);
  }

  Circle(Vector const & o, double r) :_o(o),_r(r) {
    std::cout << "Circle" <<std::endl;
  }

  double area() const{
    return M_PI * _r * _r;
  }

  double length() const{
    return 2 * M_PI * _r;
  }

  Circle * clone() const {
    return new Circle(*this);
  }


  ~Circle () {
  std::cout << "~Circle" <<std::endl;
}

private:
  Vector _o;
  const double _r;
};

#endif
