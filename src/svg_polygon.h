#ifndef SVG_POLYGON_H
#define SVG_POLYGON_H

class SvgPolygon : public SvgShape {
public:
  SvgPolygon(std::vector<double *> const & _v):SvgShape(), v(_v){}

  std::string toSVG() const {
    return std::string("<polygon points=\"" + std::to_string(v[0][0]) + " " + std::to_string(v[0][1]) + " "
    + std::to_string(v[1][0]) + " " + std::to_string(v[1][1]) + " " + std::to_string(v[2][0]) + " " + std::to_string(v[2][1])
    + "\" stroke-width=\""+std::to_string(width)+"\" stroke=\""+strokeColor+"\" fill=\""+fill+"\"/>");
  }

  private:
  std::vector<double *> v;
  };



#endif
