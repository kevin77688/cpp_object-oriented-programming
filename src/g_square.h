#ifndef G_SQUARE_H
#define G_SQUARE_H

class GSquare{
public:
  GSquare(double s) : _s(s){}

  double area() const{
    return _s * _s;
  }

  double perimeter() const{
    return 4 * _s;
  }
private:
  double _s;

};

#endif
