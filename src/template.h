#ifndef TEMPLATE_H
#define TEMPLATE_H

template<class Type>
void fill(Type * const array, int size, Type element) {
  for(int i=0;i<size;i++) {
    array[i]=element;
  }
}

template<class Type>
void reverse(Type * const array, int size) {
  Type a[size-1];
  for(int i=0;i<size;i++)
    a[size-1-i] = array[i];
  for(int j=0;j<size;j++)
    array[j] = a[j];
}

template<class Type>
Type * deepCopy(Type * const array, int size) {
  Type *copied = new Type [size];
  for(int i=0;i<size;i++)
    copied[i] = array[i];
  return copied;
}

#endif
