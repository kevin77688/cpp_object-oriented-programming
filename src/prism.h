#ifndef PRISM_H
#define PRISM_H

class Prism {
public:
  Prism(Shape * _s, double _h) {
    ss = _s->clone();
    _height = _h;
  }

  double volume() const {
    return (ss->area() * _height);
  }

  double height() const {
    return _height;
  }

  double surfaceArea() const {
    return (2 * ss->area() + ss->length() * _height);
  }

private:
  double _height;
  Shape * ss;
};


#endif
