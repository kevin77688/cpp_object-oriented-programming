#ifndef BOOK_H
#define BOOK_H

class Book {
public:
  Book() {
    //
  }
  Book(std::string const & a, std::string const & b, int p){
    _title = a;
    _author = b;
    if(p < 0)
      throw "Input Error !";
    else
      _price = p;
  }
  std::string title() const {
    return _title;
  }
  std::string author() const {
    return _author;
  }
  int price() const {
    return _price;
  }
  bool operator == (Book const & k) const {
    if(_title == k.title() && _author == k.author() && _price == k.price())
      return true;
    else
      return false;
  }
  void operator = (Book const & a) {
    _title = a.title();
    _author = a.author();
    _price = a.price();
  }
private:
  std::string _title;
  std::string _author;
  int _price;
};

#endif
