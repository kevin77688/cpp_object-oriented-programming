#ifndef COMBO_H
#define COMBO_H

#include "shape.h"
#include <list>
#include <algorithm>

class Combo : public Shape {
public:
  double area() const {
    double result = 0;
    for(Shape * i:_components) {
      result += i->area();
    }
    return result;
  }
  double length() const {
    double result = 0;
    for(Shape * i:_components) {
      result += i->length();
    }
    return result;
  }

  void add(Shape * s) {
    _components.push_back(s);
  }

  void remove(Shape * s) {
    auto it = std::find(_components.begin(),_components.end(),s);
    if(it == _components.end())
      throw std::string("Not Found 404");
    _components.erase(it);
  }

  Combo * clone() const {
    return new Combo(*this);
  }

private:
  std::list<Shape *> _components;
};

#endif
