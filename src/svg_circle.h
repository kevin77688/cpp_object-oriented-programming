#ifndef SVG_CIRCLE_H
#define SVG_CIRCLE_H
#include "svg_shape.h"

class SvgCircle : public SvgShape {
public:
    SvgCircle(double _x, double _y, double _r):SvgShape(), x(_x), y(_y), r(_r){}
    std::string toSVG() const {
      return std::string("<circle cx=\"" + std::to_string(x) + "\" cy=\"" + std::to_string(y) + "\" r=\"" + std::to_string(r)
      + "\" stroke-width=\"" + std::to_string(width) + "\" stroke=\"" + strokeColor +  "\" fill=\"" + fill + "\"/>");
    }

private:
    double x, y, r;
};



#endif
