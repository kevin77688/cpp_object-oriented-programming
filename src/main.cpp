#include "polygon.h"

#include <iostream>
#include <fstream>
#include <cstdlib>

using namespace std;

int main(int argc, char **argv) {

  if(!Check_Input(argc))
    return 1;
  // Check import valid

  int number_of_file = (argc-1);
  for (int i = 1;i<=number_of_file;i++) {
    ifstream file (argv[i]);


    std::cout << "[" << i << "]\t" << argv[i] << std::endl;
    if(!Check_File(file))
      return 1;
      //Check file status


    Polygon p = createPolygon(file);
    //Overloaded createPolygon

    cout << "Length of polygon is: " << p.length() << endl;
    cout << "Area of polygon is: " << p.area() << endl << endl;
  }
  return 0;
}
