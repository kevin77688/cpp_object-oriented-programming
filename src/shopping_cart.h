#ifndef SHOPPING_CART_H
#define SHOPPING_CART_H
#include "book.h"

class ShoppingCart {
public:
  ShoppingCart() {
    _item = 0;
    _totalPrice = 0;
    _book = nullptr;
  }

  ShoppingCart(Book const * const a, int b) {
    _item = b;
    _totalPrice = 0;
    _book = new Book[100];

    for(int i = 0; i<b ; i++)
      _book[i] = a[i];
  }

  ~ShoppingCart() {
    delete [] _book;
  }

  void add(Book const & a) {
    _item++;

    if(_book==nullptr){
      delete [] _book;
      _book = new Book [100];
    }

    for(int i = 0; i<100;i++) {
      if(a ==_book[i])
        throw "In Cart !";
    }

    _book[_item-1] = a;

  }

  void del(Book const & a) {
    for(int i = 0; i<100;i++) {
      if(a==_book[i]) {
        _item--;

        for(int m = i; m<100;m++)
          _book[i] = a;

        return;
      }
    }

    throw "UnFinded";
  }

  int numberOfItems() const {
    return _item;
  }

  Book getBookByTitle (std::string const & a) {
    for(int i = 0;i<100;i++) {
      if(_book[i].title() == a)
        return _book[i];
    }
    throw "UnFinded";
  }

  int totalOfPrice() {
    for(int i = 0 ; i < _item ; i++) {
      _totalPrice += _book[i].price();
    }
    return _totalPrice;
  }

private:
  int _item;
  int _totalPrice;
  Book *_book;
};

#endif
