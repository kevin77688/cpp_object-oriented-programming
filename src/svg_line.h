#ifndef SVG_LINE_H
#define SVG_LINE_H

class SvgLine : public SvgShape {
public:
  SvgLine(double _x1, double _y1, double _x2, double _y2):SvgShape(), x1(_x1), y1(_y1), x2(_x2), y2(_y2){

  }
  std::string toSVG() const {
    return  std::string("<line x1=\"" + std::to_string(x1) + "\" y1=\"" + std::to_string(y1)
    + "\" x2=\"" + std::to_string(x2) + "\" y2=\"" + std::to_string(y2)
    + "\" stroke-width=\""+std::to_string(width)+"\" stroke=\""+strokeColor+"\" fill=\"" + fill + "\"/>");
  }

  private:
      double x1, y1, x2, y2;
  };


#endif
