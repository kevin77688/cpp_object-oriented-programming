#ifndef POLYGON_H
#define POLYGON_H

#include "./vector.h"
#include "./dot.h"
#include "./bubblesort.h"
#include "./shape.h"
#include <iostream>
#include <fstream>
#include <cstdlib>

class Polygon : public Shape{
public:
  Polygon(Vector vertices[], int numberOfVertices):
  Shape(),
  _numberOfVertices(numberOfVertices),
  _vertices(new Vector [numberOfVertices]) {
    std::cout <<"Polygon" <<std::endl;
    for (int i=0; i< _numberOfVertices; ++i)
      _vertices[i] = vertices[i];
  }

  Polygon():_numberOfVertices(-1),_vertices(nullptr){}

  Polygon(Polygon const & p):
  _numberOfVertices(p._numberOfVertices),
  _vertices(new Vector [p._numberOfVertices]){
    for (int i=0; i< _numberOfVertices; ++i)
      _vertices[i] = p._vertices[i];
  }

  Vector & vertex(int index) const {
    return _vertices[index-1];
  }


  ~Polygon (){
    delete [] _vertices;
    std::cout << "~Polygon" <<std::endl;
  }

  double length() const {
    double totalLength=0;
    for(int i=1;i<_numberOfVertices;i++){
      totalLength+=(vertex(i+1)-vertex(i)).length();
    }
    totalLength+=(vertex(1)-vertex(_numberOfVertices)).length();
    return totalLength;
  }

  double area() const {
    double result = 0;
    for (int i = 2; i < _numberOfVertices; ++i){
      result += triangleArea(vertex(1),vertex(i),vertex(i+1));
    }
    return result;
  }

  Polygon &operator=(Polygon const & p){
    if(_vertices)
      delete [] _vertices;
    _numberOfVertices = p._numberOfVertices;
    _vertices = new Vector [_numberOfVertices];
    for (int i=0; i< _numberOfVertices; ++i)
      _vertices[i] = p._vertices[i];
    return * this;
  }

  Polygon * clone() const {
    return new Polygon(*this);
  }

private:
  Vector * _vertices;
  int _numberOfVertices;
};

Polygon createPolygon(Vector vertices[],int size){
  Vector oo = centroid(vertices,size);
  Vector r = vertices[0] - oo;
  bubbleSort(vertices,vertices+size,
    [&](Vector u, Vector v) {return angle(r,u-oo)>angle(r,v-oo);}
  );
  return Polygon (vertices,size);
}


bool Check_Input (int numbers) {
  if(numbers < 2){
    std::cout << "Input Error !" <<std::endl;
    return false;
  }
  else
    return true;
}

bool Check_File(std::ifstream &file) {
  if(!file) {
    std::cout << "Open File Failed !" <<std::endl;
    return false;
  }
  else
    return true;
}

Polygon createPolygon(std::ifstream &file) {
  int num_of_vertices = 0;
  char ch;
  file >> num_of_vertices;

  Vector * vertices;
  vertices = new Vector [num_of_vertices];

  for(int i = 0; i < num_of_vertices;i++) {
    double * comp;
    comp = new double [2];
      file >> ch >> comp[0] >> ch >> comp[1] >> ch;
    vertices[i] = Vector(comp,2);
  }

  Polygon *p = new Polygon (vertices,num_of_vertices);
  return *p;
}

#endif
