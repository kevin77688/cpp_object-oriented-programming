#ifndef STUDENT_H
#define STUDENT_H
#include <vector>
#include <map>
#include "bubblesort.h"

class Student {
public:
  virtual std::string id() const = 0;
  virtual std::string name() const = 0;
  virtual void setScore(std::string courseName, int score) = 0;
  virtual int getScore(std::string courseName) = 0;
  virtual std::string catagory() = 0;
};

class Undergrad : public Student {
public:
  Undergrad(std::string ID, std::string name) : _ID(ID), _name(name) {}

  std::string id() const {
    return _ID;
  }

  std::string name() const {
    return _name;
  }

  void setScore(std::string courseName, int score) {
    _score[courseName] = score;
  }

  int getScore(std::string courseName) {
    return _score[courseName];
  }

  std::string catagory() {
    return "Undergrad";
  }

private:
  std::string _ID, _name;
  std::map<std::string, int> _score;
};

class Grad : public Student {
public:
  Grad(std::string ID, std::string name) : _ID(ID), _name(name) {}

  std::string id() const {
    return _ID;
  }

  std::string name() const {
    return _name;
  }

  void setScore(std::string courseName, int score) {
    _score[courseName] = score;
  }

  int getScore(std::string courseName) {
    return _score[courseName];
  }

  std::string catagory() {
    return "Grad";
  }

private:
  std::string _ID, _name;
  std::map<std::string, int> _score;
};



class Course {
public:
  Course() {}
  Course(std::string ID, std::string name) : _ID(ID), _name(name) {}

  std::string id() const {
    return _ID;
  }

  std::string name() const {
    return _name;
  }

  void add(Student * s) {
    _ss.push_back(s);
    s -> setScore(_name,-1);
  }

  void add(Student ** f, Student ** s) {
    for(int i = 0 ; i < (s-f) ; i++) {
      _ss.push_back(f[i]);
      f[i] -> setScore(_name,-1);
    }
  }

  int size() {
    return _ss.size();
  }

  std::vector<Student *> getStudentsByNameInc() {
    bubbleSort(_ss.begin(),_ss.end(),
      [&](Student * a, Student * b) {
        for(int i = 0; i < 10 ; i++) {
          if(a->name().at(i) != b -> name().at(i)) {
            return a->name().at(i) > b -> name().at(i);
            break;
          }
        }
      }
    );
    return _ss;
  }

  void setScores (std::vector<std::string> ids, std::vector<int> score) {
    for(int j = 0 ; j < (ids.end()-ids.begin()) ; j++) {
      for(int i = 0 ; i < (_ss.end()-_ss.begin()) ; i++) {
        if(_ss.at(i)->id().compare(ids.at(j)) == 0) {
          _ss.at(i) -> setScore(_name, score.at(j));
        }
      }
    }

  }

  std::vector<Student *> failedStudents() {
    for(int i = 0 ; i < (_ss.end()-_ss.begin()) ; i++) {
      if((_ss.at(i)->catagory().compare("Undergrad") == 0) && (_ss.at(i)->getScore(_name) < 60)) {
        _fs.push_back(_ss.at(i));
      }
      else if((_ss.at(i)->catagory().compare("Grad") == 0) && (_ss.at(i)->getScore(_name) < 70)) {
        _fs.push_back(_ss.at(i));
      }
    }
    return _fs;
  }

private:
  std::string _ID, _name;
  std::vector<Student *> _ss;
  std::vector<Student *> _fs;
};


#endif
