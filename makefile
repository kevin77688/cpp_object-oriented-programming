.PHONY: directories clean
CC=g++
CFLAGS=-std=c++11
LIBS=-lgtest -lpthread
OBJ=obj
BIN=bin
SRC=src
TEST=test

all: clean directories $(BIN)/ut_all $(BIN)/dot_product $(BIN)/polygon

$(BIN)/dot_product: $(SRC)/user_main.cpp
	$(CC) $(CFLAGS) -o $@ $<

$(BIN)/polygon: $(SRC)/main.cpp 
	$(CC) $(CFLAGS) -o $@ $<

$(BIN)/ut_all: $(OBJ)/ut_main.o
	$(CC) $(CFLAGS) -o $@ $(OBJ)/ut_main.o $(LIBS)

$(OBJ)/ut_main.o: $(TEST)/ut_main.cpp
	$(CC) $(CFLAGS) -c $< -o $@

directories:
	mkdir -p bin obj

clean:
	rm -rf $(OBJ) $(BIN)

stat:
	wc $(SRC)/*.h $(SRC)/*.cpp $(TEST)/*.h $(TEST)/*.cpp
