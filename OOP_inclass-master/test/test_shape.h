#include "../src/square.h"
#include "../src/circle.h"
#include "../src/polygon.h"
#include "../src/combo.h"
#include "../src/bubblesort.h"

TEST (ShapeTest, Square) {
  double s = 2;
  Square p(s);
  ASSERT_EQ(4, p.area());
  ASSERT_EQ(8, p.length());
}

TEST(ShapeTest, Factory) {
  Circle c1 = Circle::create(1,-1,1);
  ASSERT_EQ(M_PI,c1.area());
}

TEST(ShapeTest, Car) {
  Circle c1 = Circle::create(1,-1,1);
  Circle c2 = Circle::create(3,-1,1);
  double a[] = {0,0};
  double b[] = {4,0};
  double c[] = {4,2};
  double d[] = {0,2};
  Vector vertices[] = {Vector(a,2),Vector(b,2),Vector(c,2),Vector(d,2)};
  Polygon p = createPolygon(vertices,4);
  Square s(2);
  Combo combo;
  combo.add(&c1);
  combo.add(&c2);
  combo.add(&p);
  combo.add(&s);
  ASSERT_EQ(2 * M_PI + 12,combo.area());
  ASSERT_EQ(4 * M_PI + 20,combo.length());
  combo.remove(&c1);
  ASSERT_EQ(M_PI + 12,combo.area());
  ASSERT_EQ(2 * M_PI + 20,combo.length());
  ASSERT_ANY_THROW(combo.remove(&c1));
}

TEST(ShapeTest,sorting) {
  Circle c1 = Circle::create(1,-1,1);
  Circle c2 = Circle::create(3,-1,1);
  double a[] = {0,0};
  double b[] = {4,0};
  double c[] = {4,2};
  double d[] = {0,2};
  Vector vertices[] = {Vector(a,2),Vector(b,2),Vector(c,2),Vector(d,2)};
  Polygon p = createPolygon(vertices,4);
  Square s(2);
  Combo combo;
  combo.add(&c1);
  combo.add(&c2);
  combo.add(&p);
  combo.add(&s);
  Shape * ss[] = {&c1, &p, &c2, &s, &combo};
  bubbleSort(ss,ss+5,[](Shape * a, Shape * b){
    return a->area() > b->area();
  });
  ASSERT_EQ(&c1,ss[0]);
  ASSERT_EQ(&c2,ss[1]);
  ASSERT_EQ(&s,ss[2]);
  ASSERT_EQ(&p,ss[3]);
  ASSERT_EQ(&combo,ss[4]);
}
