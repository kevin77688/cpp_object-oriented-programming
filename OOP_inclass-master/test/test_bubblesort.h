
#include "../src/bubblesort.h"
#include "../src/dot.h"
#include "../src/vector.h"
#include <string>
#include <cmath>
#include <vector>
#include <list>

TEST (bubblesort, integer) {
  int a[] = {3, 5, 1, 2, 6};
  bubbleSort(a,a+5);
  ASSERT_EQ(1,a[0]);
  ASSERT_EQ(3,a[2]);
  ASSERT_EQ(6,a[4]);
}

TEST (bubblesort, _float) {
  double a[] = {3.1, 5.1, 1.1, 2.1, 6.1};
  bubbleSort(a,a+5);
  ASSERT_EQ(1.1,a[0]);
  ASSERT_EQ(3.1,a[2]);
  ASSERT_EQ(6.1,a[4]);
}

TEST (bubblesort, _floatPart) {
  double a[] = {3.1, 5.1, 1.1, 2.1, 6.1};
  bubbleSort(a+1,a+4);
  ASSERT_EQ(3.1,a[0]);
  ASSERT_EQ(2.1,a[2]);
  ASSERT_EQ(1.1,a[1]);
  ASSERT_EQ(5.1,a[3]);
  ASSERT_EQ(6.1,a[4]);
}

TEST (bubblesort, integerDescending) {
  int a[] = {3, 5, 1, 2, 6};
  bubbleSort(a,a+5,lessThan<int>);
  ASSERT_EQ(6,a[0]);
  ASSERT_EQ(3,a[2]);
  ASSERT_EQ(1,a[4]);
}

TEST (bubblesort, integerAscending) {
  int a[] = {3, 5, 1, 2, 6};
  bubbleSort(a,a+5,greaterThan<int>);
  ASSERT_EQ(1,a[0]);
  ASSERT_EQ(3,a[2]);
  ASSERT_EQ(6,a[4]);
}

TEST (bubblesort, integerAscendingLambda) {
  int a[] = {3, 5, 1, 2, 6};
  bubbleSort(a,a+5,
    [](int a, int b) {return a>b;}
  );
  ASSERT_EQ(1,a[0]);
  ASSERT_EQ(3,a[2]);
  ASSERT_EQ(6,a[4]);
}

TEST (bubblesort, distanceToFiveAscending) {
  int a[] = {3, 5, 1, 2, 6};
  bubbleSort(a,a+5,
    [](int a, int b) {return abs(5-a)>abs(5-b);}
  );
  ASSERT_EQ(5,a[0]);
  ASSERT_EQ(3,a[2]);
  ASSERT_EQ(1,a[4]);
}

TEST (bubblesort, swapInt) {
  int a = 1, b = 2;
  swap<int>(a, b);
  ASSERT_EQ(2, a);
  ASSERT_EQ(1, b);

}

TEST (bubblesort, swapDouble) {
  double a = 1.1, b = 2.1;
  swap<double>(a, b);
  ASSERT_EQ(2.1, a);
  ASSERT_EQ(1.1, b);

}

TEST (bubblesort, distanceToXAscending){
  int a[] = {3, 5, 1, 2, 6};
  int x = 5;
  bubbleSort(a,a+5,
    [&](int a, int b) {return abs(x-a)>abs(x-b);}
  );
  ASSERT_EQ(5,a[0]);
  ASSERT_EQ(3,a[2]);
  ASSERT_EQ(1,a[4]);
}

TEST (bubblesort, distanceToXAscendingOnVector){
  int aa[] = {3, 5, 1, 2, 6};
  std::vector<int> a(aa, aa+5);
  ASSERT_EQ(3, a[0]);
  auto it = a.begin();
  ASSERT_EQ(3, *it);
  int x = 5;
  bubbleSort(a.begin(),a.begin()+5,
    [&](int a, int b) {return abs(x-a)>abs(x-b);}
  );
  ASSERT_EQ(5,a[0]);
  ASSERT_EQ(3,a[2]);
  ASSERT_EQ(1,a[4]);
}

TEST(bubblesort,swapString) {
  std::string a="Hello",b="World.";
  ::swap<std::string>(a,b);
  ASSERT_EQ("World.",a);
  ASSERT_EQ("Hello",b);
}

TEST(bubblesort, sortVertices){
    double v1[] = {0,0};
    double v2[] = {3,4};
    double v3[] = {0,4};
    double v4[] = {3,0};
    Vector a( v1, 2);
    Vector b( v2, 2);
    Vector c( v3, 2);
    Vector d( v4, 2);
    Vector vertices[] = {a,b,c,d};
    Vector oo = centroid(vertices,4);
    Vector r = a - oo;
    bubbleSort(vertices,vertices+4,
      [&](Vector u, Vector v) {return angle(r,u-oo)>angle(r,v-oo);}
    );
    ASSERT_EQ(0,vertices[0].at(1));
    ASSERT_EQ(0,vertices[0].at(2));
    ASSERT_EQ(3,vertices[1].at(1));
    ASSERT_EQ(0,vertices[1].at(2));
    ASSERT_EQ(3,vertices[2].at(1));
    ASSERT_EQ(4,vertices[2].at(2));
    ASSERT_EQ(0,vertices[3].at(1));
    ASSERT_EQ(4,vertices[3].at(2));
}

TEST(TestFind,VectorAndList) {
  std::vector<int> myvector = {1,2,3,4,5};
  ASSERT_EQ(myvector.end(),std::find(myvector.begin(),myvector.end(),6));
  ASSERT_EQ(myvector.begin()+2,std::find(myvector.begin(),myvector.end(),3));
  std::list<int> mylist = {2,3,4,5,6};
  ASSERT_EQ(mylist.end(),std::find(mylist.begin(),mylist.end(),1));
  // ASSERT_EQ(mylist.begin()+4,std::find(mylist.begin(),mylist.end(),6));
}
