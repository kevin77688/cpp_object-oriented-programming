# OOP with C++ (and more)
#### Spring, 2018
#### Prof Y C Cheng
#### Dept. of Computer Science and Information Engineering
#### Taipei Tech

## Week 18:

__Final: Wed 6/27, 4:10 pm - 7:10 pm (or 5:10 pm - 8:10 pm) @Computer room, 12F__

Usual rule applies.

## Week 17:

- Separating definition from implementation
- Adding iterator support to Combo
- What are friends for?

## Week 16:

Multiple inheritance: adapting G_SQUARE

Composite Shapes: A shape that is made of other shapes

## Week 15:

cleanup: improving code for the Shape example
- constructor member initialization list
- constant member function: area() const
- virtual destructor in Shape
- overloading stream insertion operator for Vector
- overloading stream extraction operator for Vector

## Week 14:

[Polymorphism](http://htsicpp.blogspot.tw/2014/12/polymorphism.html)

container-iterator-algorithm
algorithm: find

## Week 13:

[Polymorphism](http://htsicpp.blogspot.tw/2014/12/polymorphism.html)

Midterm II - Wed 5/23, 2:10 pm to 4:00 pm, in PC lab on 12F of the Tech Building

## Week 12:

Sorting vertices of a convex polygon using our bubble sort functions

Cleanup time!

[file stream](http://www.cplusplus.com/reference/fstream/ifstream/ifstream/)

[command line arguments: argc and argv](http://www.cplusplus.com/articles/DEN36Up4/)

## Week 11:

Sorting vertices of a convex polygon using our bubble sort functions
```c++
template<typename T, typename C>
void bubbleSort(T a, T b, C comp) {
  int n = b-a;
  for(int i = n-1; i > 0; i--) {
    for(int j=0;j<i;j++) {
      if (comp(a[j],a[j+1])) {
        swap(a[j],a[j+1]);
      }
    }
  }
}
```

## Week 10:

Sorting vertices of a convex polygon

Maximizing reuse: an example bubble sort
- [function templates](http://www.cplusplus.com/doc/oldtutorial/templates/)
- function as template parameter
- [lambda](https://www.cprogramming.com/c++11/c++11-lambda-closures.html)
- object as template parameter

## Week 9: MIDTERM

Wed 4/25, 1:10 pm to 4:00 pm, in PC lab on 12F of the Tech Building

## Week 8:

length and area of polygonL assuming vertices are correctly ordered

Midterm: online programming test will take place in computer lab of 12F, Tech Building from 1:10 pm to 3:00 pm, April 25, 2018.

No smartphone, social networking (LINE, FB, IG, etc.) during test

- deep copy trio for Vector
  - copy constructor
  - destructor, and
  - overloaded assignment operator

## Week 7:

[***Convex polygon***](http://htsicpp.blogspot.tw/2014/10/convex-polygon.html)

***Language:***

```c++
  Vector & vertex(int i) {...} //return a reference to Vector
  int dim() const {...}        // dim is a constant member function
  Vector operator - (Vector const & u, Vector const & v); // operator overloading
                                                         // v is a reference to a constant vector:
```
Reading: [**Matrix**](http://htsicpp.blogspot.tw/2014/09/entering-matrix.html)

## Week 6:

Vector as object

[**Inner product, round 4: refactoring into object**](http://htsicpp.blogspot.tw/2014/09/inner-product-round-4-refactoring-into.html)

C++ language
- Runtime memory model
- Call by value and call by reference
- copy constructor and destructor
- deep copy and shallow copy

## Week 5:

Vector as object

[**Inner product, round 4: refactoring into object**](http://htsicpp.blogspot.tw/2014/09/inner-product-round-4-refactoring-into.html)

Refactoring

## Week 4:

Working on the main program
- operator new and operator new []
- std::getline(std::cin, line)
- _Looking back_

Vector as object

[**Inner product, round 4: refactoring into object**](http://htsicpp.blogspot.tw/2014/09/inner-product-round-4-refactoring-into.html)

## Week 3:

[**How to Solve It**](http://htsicpp.blogspot.tw/2014/08/introducing-how-to-solve-it-cpp.html):
- _Understanding the problem_: by constructing an example
- _Devising a plan_: what we must do to make the example work?
- _Carrying out the plan_: test, code, refactor
- _Looking back_
Our plan for the problem of computing inner product

- Compute inner product
- input/output
  - understanding string
  - understanding iostream objects cin/cout

[**Inner product, round 3: handling I/O**](http://htsicpp.blogspot.tw/2014/08/inner-product-round-3-handling-io.html)

## Week 2:

### reading:

- [Inner product, round 1](http://htsicpp.blogspot.tw/2014/08/inner-product-round-1.html)

- [Inner product, round 2: unit tests and basic exception](http://htsicpp.blogspot.tw/2014/08/inner-product-round-2-unit-tests-and.html)

### C++ language & tool

- When coding in class, I will use the editor [Atom](https://atom.io), which comes with syntax highlighting, code completion to make coding easy. I will also use the plugin [PlatformIO IDE Terminal](https://atom.io/packages/platformio-ide-terminal) so that we can access a terminal to build programs without leaving Atom.

- make and makefile
- exception in c++: why assert from assert.h is not good: forced exit if failed

### problem solving

We are going to solve this problem
- all code together
- separate entry point (main), functions, and tests
- extract tests under gtest

**Problem**. Prompt the user to input two vectors. Compute the inner product (or dot product) of two vectors when it is defined. For example,

    [1, 0] · [1, 1] = 1,
    [1, 1, 0] · [0, 1, 1] = 1, and
    [1,0] · [1,1,0] => undefined.

Prompt the user whether to continue or stop.

More detail see [Inner product, round 1](http://htsicpp.blogspot.tw/2014/08/inner-product-round-1.html)

## Week 1

_Incrementally_ from small (tiny) programs to not-so-small programs

- Simple tooling
  - From IDE Code::Blocks, Dev, Visual Studio, ...
  - to simple tools Atom, g++, make
- Organizing project directory: We will work with projects with many files
  - From:
```
  hello.cpp a.out
```
  - To:
```
  src/hello.cpp      // folder for production code
  bin/hello          // folder for executables
  test/              // folder for test code
  obj/               // folder for object code
  makefile
  readme.md // project info
```
- makefile objectives:
  - maintaining project structure
  - enabling recompilation when dependent files changes
  - actions:
    - compile
    - link
    - cleanup
    - statistics
- jenkins/gitlab/googletest
- Reading and writing [htsi](http://htsicpp.blogspot.tw/):
  - [Introducing "How to solve it: CPP"](http://htsicpp.blogspot.tw/2014/08/introducing-how-to-solve-it-cpp.html)
