#ifndef VECTOR_H
#define VECTOR_H

#include <cmath>

class Vector {
public:
  Vector (double * a, int dim) {
    _dim = dim;
    _comp = new double [_dim];
    for (int i=0; i<_dim; ++i)
      _comp[i] = a[i];
  }

  Vector(){//default constructor
    _comp = nullptr;
    _dim = -1;
  }

  Vector (Vector const & v) { // deep copy constructor
    _dim = v._dim;
    _comp = new double [_dim];
    for (int i=0; i<_dim; ++i)
      _comp[i] = v._comp[i];
  }

  ~Vector() { // destructor with deep copy semantics
    delete [] _comp;
  }

  int dim() const {
    return _dim;
  }

  double at(int index) const {
    return _comp[index-1];
  }

  double length() const {
    double result=0;
    for(int i=0;i<_dim;i++){
      result+=_comp[i]*_comp[i];
    }
    return sqrt(result);
  }

  Vector & operator= (Vector const &u){
    if(_comp)
      delete [] _comp;
    _comp = new double[u._dim];
    _dim = u._dim;
    for (int i=0; i<u._dim; ++i)
      _comp[i] = u._comp[i];

    return *this;
  }

  Vector * getThis () {
    return this;
  }

  Vector * operator &() {
    return this;
  }

private:
  double * _comp;
  int _dim;
};

Vector operator- (Vector const &u, Vector const &v){
  double comp[u.dim()];

  for(int i=0 ; i<u.dim() ; i++){
    comp[i]=u.at(i+1)-v.at(i+1);
  }

  Vector result(comp,u.dim());

  return result;
}

double triangleArea(Vector const & a, Vector const & b, Vector const & c){
  double A = (a-b).length();
  double B = (b-c).length();
  double C = (c-a).length();
  double s = (A+B+C)/2;
  return sqrt(s*(s-A)*(s-B)*(s-C));
}



#endif
