#ifndef VECTORIO_H
#define VECTORIO_H

#include <sstream>
#include <string>
#include <iostream>

#include "vector.h"

double * pullVectorFromString(std::string s, int * dim) {
  std::stringstream ss(s);
  ss >> *dim;
  char ch;
  ss >> ch;
  double *v = new double [*dim];
  for(int i=0 ; i<*dim ; i++){
    ss >> v[i] >> ch;
  }
  return v;
}

Vector * pullVectorFromString(std::string s) {
  std::stringstream ss(s);
  int dim;
  ss >> dim;
  char ch;
  ss >> ch;
  double *v = new double [dim];
  for(int i=0 ; i<dim ; i++){
    ss >> v[i] >> ch;
  }
  Vector * vec = new Vector(v, dim);
  delete [] v;
  return vec;
}

bool pullContinueFromString(std::string s) {
  std::stringstream ss(s);
  char ch;
  ss >>  ch;
  return ch == 'c';
}

double * promptVectorFromUser(int &dim)
{
    std::string line;
    std::cout << "Input a vector in the following format \"n [c1,...,cn]\": ";
    std::getline(std::cin, line);
    std::cout << "Your input:" << line << std::endl;
    double * vec = pullVectorFromString(line, &dim);

    return vec;
}

Vector * promptVectorFromUser()
{
    std::string line;
    std::cout << "Input a vector in the following format \"n [c1,...,cn]\": ";
    std::getline(std::cin, line);
    std::cout << "Your input:" << line << std::endl;
    return pullVectorFromString(line);
}

bool promptContinueFromUser()
{
    std::string line;
    std::cout << "Type c<ret> to continue, anything else followed by <ret> to terminate: ";
    std::getline(std::cin, line);

    return pullContinueFromString(line);
}

std::ostream & operator << (std::ostream & os, Vector const & vec) {
  // std::string s =
  os << vec.dim() << " [" ;
  for (int i = 1; i<vec.dim() ; i++) {
    os << vec.at(i) << ", ";
  }
  os << vec.at(vec.dim()) << "]";
  return os;
}

std::istream & operator >> (std::istream & is, Vector & vec) {
  int dim;
  is >> dim;
  char ch;
  is >> ch;
  double *v = new double [dim];
  for(int i=0 ; i<dim ; i++){
    is >> v[i] >> ch;
  }
  Vector w(v,dim);
  vec = w;
  delete [] v;
  return is;
}

#endif
