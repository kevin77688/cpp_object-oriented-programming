#ifndef DOT_H
#define DOT_H
#include "./vector.h"
#include <cmath>
#include <string>

double dotProduct(Vector const & u, Vector const & v) {
  if(u.dim() != v.dim())
    throw std::string("Dimension error");
  double answer = 0;
  for(int i=1;i<=u.dim();i++){
    answer += u.at(i)*v.at(i);
  }
  return answer;
}

Vector centroid(Vector vertices[], int size) {
  double x = 0, y = 0;
  for(int i=0;i<size;i++) {
    x+=vertices[i].at(1);
    y+=vertices[i].at(2);
  }
  double temp[] = {x/size,y/size};
  Vector result(temp, 2);
  return result;
}

bool pointingOut(Vector const & u, Vector const & v){
  return u.at(1)*v.at(2)-u.at(2)*v.at(1)>=0;
}

double angle(Vector  const & u, Vector  const & v) {
  if(pointingOut(u,v))
    return acos(dotProduct(u,v)/(u.length()*v.length()));
  else
    return 2*M_PI-acos(dotProduct(u,v)/(u.length()*v.length()));
}


#endif
