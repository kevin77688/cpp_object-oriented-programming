#ifndef SHAPE_H
#define SHAPE_H

class Shape {
public:
  // Shape () {
  //   std::cout << "Shape" <<std::endl;
  // }
  Shape (int i=1) {
    std::cout << "Shape i" <<std::endl;
  }
  virtual double area() const = 0; // pure virtual
  virtual double length() const = 0;

  virtual ~Shape() {
    std::cout << "~Shape" <<std::endl;
  }
};

#endif
