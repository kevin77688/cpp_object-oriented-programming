#ifndef SHELF_H
#define SHELF_H

#include "./book.h"
#include <vector>

class Shelf {
public:
  void add(Book * b){
    _sh.push_back(b);
    b->setShelf(this);
  }
private:
  std::vector <Book *> _sh;
};

#endif
