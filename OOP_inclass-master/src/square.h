#ifndef SQUARE_H
#define SQUARE_H

#include "./shape.h"
#include "./g_square.h"

class Square : public Shape, private GSquare{
public:
  Square(double s) : GSquare(s), Shape(1){}

  double area() const{
    return GSquare::area();
  }

  double length() const{
    return perimeter();
  }
};

#endif
